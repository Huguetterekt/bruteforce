# bruteForce

Mon Projet Bruteforce

## Getting started

Ce projet a pour but de forcer la connexion à un site web ( ici site Wordpress ) en testant un grand nombre de possibilités, parmi des noms d'utilisateurs et des mots de passe.

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Huguetterekt/bruteforce.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:f35a578dc04fd9a3a1b5c32d05b4fc59?https://docs.gitlab.com/ee/user/clusters/agent/)


## Name
Mon projet Bruteforce

## Description
Ce projet a pour but de forcer la connexion à un site web ( ici site Wordpress ) en testant un grand nombre de possibilités, parmi des noms d'utilisateurs et des mots de passe.


## Installation

Il est nécessaire d'avoir la version 16 de node js d'installer afin d'éxécuter le script bruteforce. Afin d'éxécuter le script, "node bruteforce.js".

## Usage
Node.js

## Support
Cyril


## Contributing
MBA 1 Dév 

## Authors and acknowledgment
Nicolas LE GALL



